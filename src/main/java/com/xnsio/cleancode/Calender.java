package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Calender {
    Map<LocalDate, List<Integer>> calenderSchedule = new HashMap<>();

    public boolean book(LocalDate date, int time) {
        return isSlotFree(date,time);
    }

    private boolean isSlotFree(LocalDate date, int time) {
    if(calenderSchedule.get(date)!=null)
        return !calenderSchedule.get(date).contains(time);
    else
        return true;
    }
}
