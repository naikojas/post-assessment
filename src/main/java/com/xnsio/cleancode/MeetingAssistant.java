package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MeetingAssistant {
    private List<Integer> WORKINGHOURS;
    private List<Integer> forFreeHours;
    private List<Integer> withFreeHours;
    private List<Integer> availableSlots;
    public MeetingAssistant(List<Integer>workingHours) {
        WORKINGHOURS = workingHours;
    }

    public Boolean canBookMeeting(Calender meetingFor, Calender meetingWith, LocalDate date) {
        if (setFreeCalenderHours(meetingFor, meetingWith, date)) return false;

        availableSlots = matchslots(forFreeHours, withFreeHours);
        return !(availableSlots.isEmpty());
    }

    private boolean setFreeCalenderHours(Calender meetingFor, Calender meetingWith, LocalDate date) {
        if (meetingFor.calenderSchedule.isEmpty()||meetingFor.calenderSchedule.get(date)==null)
            return true;
        if (meetingWith.calenderSchedule.isEmpty()||meetingWith.calenderSchedule.get(date)==null)
            return true;
        forFreeHours = new ArrayList<>(WORKINGHOURS);
        forFreeHours.removeAll(meetingFor.calenderSchedule.get(date));
        withFreeHours = new ArrayList<>(WORKINGHOURS);
        withFreeHours.removeAll(meetingWith.calenderSchedule.get(date));
        return false;
    }

    public int BookMeeting(Calender meetingFor, Calender meetingWith,LocalDate date,int endTime){
        if(!canBookMeeting(meetingFor,meetingWith,date))
            return 0;
        Collections.sort(availableSlots,Collections.reverseOrder());
        for (int slot:availableSlots) {
            if (slot <= endTime)
                return slot;
        }
        return 0;
    }

    private List<Integer> matchslots(List<Integer> forSlots,List<Integer> withSlots) {
       return forSlots.stream().filter(withSlots::contains).collect(Collectors.toList());
    }

    private List<Integer> getSlot(Calender meetingFor,Calender meetingWith,LocalDate date) {
        return meetingFor.calenderSchedule.get(date).stream().filter(meetingWith.calenderSchedule.get(date)::contains).collect(Collectors.toList());
    }
}
