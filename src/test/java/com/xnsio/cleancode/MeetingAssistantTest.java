package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

public class MeetingAssistantTest {
    LocalDate TODAY = LocalDate.now();
    LocalDate TOMMOROW = TODAY.plusDays(1);
    ArrayList<Integer> workingHours = new ArrayList<>(Arrays.asList(8,9,10,11,12,13,14,15,16,17));
   Calender busyBee = new Calender();
   Calender alwaysFree = new Calender();
   Calender person1 = new Calender();
    Calender person2 = new Calender();
   List<Calender>calenders = new ArrayList<>();
   MeetingAssistant meetingAssistant = new MeetingAssistant(workingHours);
    @Test
    public void shouldScheduleMeetingWithEmptyCaleder(){
        alwaysFree.calenderSchedule.put(TODAY, Collections.EMPTY_LIST);
        person1.calenderSchedule.put(TODAY,Arrays.asList(10,11,15,16));
        assertTrue(meetingAssistant.canBookMeeting(person1,alwaysFree,TODAY));

    }

    @Test
    public void shouldNotScheduleMeetingWithABusyCalender(){
        busyBee.calenderSchedule.put(TODAY,workingHours);
        busyBee.calenderSchedule.put(TOMMOROW,workingHours);
        assertFalse(meetingAssistant.canBookMeeting(person1,busyBee,TODAY));
    }

    @Test
    public void shouldReturnMeetingTimeIfslotisAvailable(){
        person1.calenderSchedule.put(TODAY,Arrays.asList(10,11,15,16));
        person2.calenderSchedule.put(TODAY,Arrays.asList(8,12,15,17));
        assertTrue(meetingAssistant.canBookMeeting(person1,person2,TODAY));
        assertEquals(14,meetingAssistant.BookMeeting(person1,person2,TODAY,16));
    }
    @Test
    public void shouldBookTimeSlotOnPreviousDayIfNoSlotAvailableOnSameDay(){

    }

}
