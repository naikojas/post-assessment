package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class CalenderTest {
    Calender calender  = new Calender();
    private static final int TEN=10;
    private static final int EIGHT = 8;
    private static final LocalDate TODAY = LocalDate.now();
    @Test
    public void calenderShouldReturnBusyIfNoSlotAvailable(){
        calender.calenderSchedule.put(TODAY, Arrays.asList(TEN));
        assertFalse(calender.book(TODAY,TEN));
    }

    @Test
    public void calendershouldBookSlotIfnotBusy(){
        assertTrue(calender.book(TODAY,EIGHT));
    }
}
